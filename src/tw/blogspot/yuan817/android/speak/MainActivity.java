/**
 * 文字轉語音
 * FileName:	MainActivity.java
 *
 * 日期: 		2012.9.4
 * 作者: 		元兒～
 * Version: 	v1.5.1
 * 更新資訊: 
 * ├─ v1.5.1 -2012.9.4
 * │  └─ 小幅修正語調、速度對應錯的問題...
 * └─ v1.5 -2012.8.22
 *    └─ 最初的版本
 * 目前Bug: 
 * └─ 
 * 預計打算:
 * └─ 建立多國語言（英文、繁體中文）
 * 
 * Description: 讓使用者輸入文字，按下按鈕後可念出使用者所輸入的字
 */
package tw.blogspot.yuan817.android.speak;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Resources;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnInitListener, OnClickListener 
{
	EditText input, tone_input, speed_input;
	Button click;
	private TextToSpeech tts;
	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        //tts =new TextToSpeech (this,this);
        
    }

	public void onInit(int status) {
		
		if(status == TextToSpeech.SUCCESS)
		{
		//設定語言為英文
			int result =tts.setLanguage(Locale.US);
			//如果該語言不見了或沒有支援則無法使用
			if(result == TextToSpeech.LANG_MISSING_DATA 
					|| result == TextToSpeech.LANG_NOT_SUPPORTED)
			{
				
				Toast.makeText(this, getResources().getText(R.string.This_Language_is_not_supported), Toast.LENGTH_SHORT).show();
				Log.e("TTS","This Language is not supported");
				
				
			} else{
				//語調(1 為正常語調; 0.5 比正常語調低一倍; 2 比正常語調高一倍)
				//tts.setPitch(1);
				
				//速度(1 為正常速度; 0.5 比正常速度慢一倍; 2 比正常速度快一倍)
				//tts.setSpeechRate((float) 1);
				//設定要說的內容文字
				//tts.speak("Hello Apple", TextToSpeech.QUEUE_FLUSH, null);	
					
				try{
					tts.setPitch(Float.parseFloat(tone_input.getText().toString()));
					tts.setSpeechRate(Float.parseFloat(speed_input.getText().toString()));
					tts.speak(""+input.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
				}
				catch(Exception ex){
					Toast.makeText(this, getResources().getText(R.string.float_input_error), Toast.LENGTH_SHORT).show();
				}
				
			}
		}else{
			Log.e("TTS","Initilization Failed!");
			Toast.makeText(this, getResources().getText(R.string.Initilization_Failed), Toast.LENGTH_SHORT).show();
			}
		
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
		if(tts!=null){
			tts.stop();
			tts.shutdown();
			
		}
	}
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.click:
			ttsSay();
		}
	}

	private void findView() {
		// TODO Auto-generated method stub
		input = (EditText) findViewById(R.id.input_text);
		click = (Button) findViewById(R.id.click);
		click.setOnClickListener(this);
		tone_input = (EditText) findViewById(R.id.tone_input);
		speed_input = (EditText) findViewById(R.id.speed_input);
	}
	private void ttsSay() {
		// TODO Auto-generated method stub
		tts =new TextToSpeech (this,this);
	}
}
